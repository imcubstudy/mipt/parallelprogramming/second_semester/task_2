// Task 2:
//     - Write a multi-threaded program which will calculate sum [1, N]
//     - Each thread must get own range to calculate partial sum
//     - bonus:
//         - N is CLI argument
//         - use omps `reduction` and `schedule` directives
// Comment: bonus task is done!
// Author: Gazizov Yakov

#include <cstdio>
#include <cstdlib>
#include <cstdint>

#include <omp.h>

int main(int const argc, char const *argv[])
{
    // get the N as argument from CLI
    int N = 0;
    if (argc != 2) {
        std::printf("Argument is not supplied! Or maybe just too mush of them...\n");
        std::exit(EXIT_FAILURE);
    }
    if (auto ret = std::sscanf(argv[1], "%d", &N); !(ret != EOF && ret == 1) || N < 1) {
        std::printf("Invalid argument was supplied!\n");
        std::exit(EXIT_FAILURE);
    }

    // set dynamic to false just in case if implementation will allocate different number of threads
    // on every other run
    omp_set_dynamic(static_cast<int>(false));

    // output maximum available number of threads
    auto const maxThreads = omp_get_max_threads();
    std::printf("Max threads %d\n", maxThreads);

    // declare full sum
    std::uint64_t sum = 0;

    // launch for loop in parallel. Each thread will accumulate partial sums that will be then automatically
    // reduced thanks to omp. Each thread will take batches of job to sum 42 numbers.
#pragma omp parallel for default(none) schedule(dynamic, 42) reduction(+: sum) firstprivate(N)
    for (int i = 1; i <= N; ++i) {
        sum += i;
    }

    // output total sum
    std::printf("Total sum: %llu\n", sum);

    return 0;
}